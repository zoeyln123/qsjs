<?php
$data = require_once __DIR__.'/data.php';
require_once __DIR__.'/common.php';
require_once __DIR__.'/lunar.php';
$fxcode = $_GET['fxcode']??null;
$result = base64_decode($fxcode);
parse_str($result,$arry);

$xing = $arry['xing']??null;
$username= $arry['username']??null;
$gender = $arry['gender']??null;
$gender = $gender==1?'男主':'女主';
$year = $arry['y']??null;
$month = $arry['m']??null;
$day = $arry['d']??null;
$birthtime = $arry['birthtime']??null;

$gongli = $year.'年'.$month.'月'.$day.'日';
$lunar = new Lunar();
$mid_ymd = $lunar->S2L($year.'-'.$month.'-'.$day);
$nongli = date("Y年m月d日",$mid_ymd);
$nongli_chinese = $lunar->LMonName(intval(date('m',$mid_ymd))).'月'.$lunar->LDayName(intval(date('d',$mid_ymd)));
$xingzuo = getConstellation($month, $day);
$weeknum = getWeekNum($year.'-'.$month.'-'.$day);

$shengchen_arr = explode('-',$birthtime);//生辰天干地支
$shengchen_start = getChineseStartTime($shengchen_arr[1]);
$shengchen_end = getChineseEndTime($shengchen_arr[2]);
$shengchen = getShengChen($shengchen_arr[0]);
$shengxiao = getShuXiang(date('Y',$lunar->S2L($year.'-'.$month.'-'.$day)));

$qid = $_GET['qid']??1;
$qsjsfx_info =require_once __DIR__.'/qsjsfx.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
<link rel="icon" href="http://storage-wechat-app.oss-cn-shenzhen.aliyuncs.com/wish/paseLife/favicon.ico">
<title><?php echo $data['show_title'];?></title>
<script src="/statics/js/limit.js?v=<?php echo time();?>" charset=utf-8></script>
<link href="/statics/wish/paseLife/css/paseLife.7f92f33e.css" rel="preload" as="style">
<link href="/statics/wish/paseLife/css/paseLife.7f92f33e.css" rel="stylesheet">
<link href="/statics/wish/paseLife/js/chunk-vendors.8e0d8f1a.js" rel="preload" as="script">
<link href="/statics/wish/paseLife/js/paseLife.26a64a32.js" rel="preload" as="script">
</head>
<body>
	<noscript>&lt;strong&gt;We're sorry but vue-cli3-demo doesn't work
		properly without JavaScript enabled. Please enable it to
		continue.&lt;/strong&gt;</noscript>
	<div id="app">
		<div data-v-5d71a960="" id="show">
			<img data-v-5d71a960="" src="/statics/wish/paseLife/images/qsjs_top.png?imageslim" onclick="return false;">
			<div data-v-5d71a960="" class="showPayMain pastLife">
				<div data-v-5d71a960="" class="info">
					<div data-v-5d71a960="" class="fl bir_img">
						<img data-v-5d71a960="" src="/statics/wish/paseLife/images/showPayTop3.png?imageslim" onclick="return false;">
						<p data-v-5d71a960="">
							<?php echo $gender;?><br data-v-5d71a960="">资料
						</p>
					</div>					
					<div data-v-5d71a960="" class="fr user-info">
						<ul data-v-5d71a960="">
							<li data-v-5d71a960=""><span data-v-5d71a960=""
								class="color_fate">姓名：</span> <?php echo $xing.$username;?>&nbsp;&nbsp;<?php echo $xingzuo;?></li>
							<li data-v-5d71a960=""><span data-v-5d71a960=""
								class="color_fate">公历：</span><?php echo $gongli;?>(<?php echo $weeknum;?>)</li>
							<li data-v-5d71a960=""><span data-v-5d71a960=""
								class="color_fate">农历：</span><?php echo $nongli;?>(<?php echo $nongli_chinese;?>)</li>
							<li data-v-5d71a960="">
							<span data-v-5d71a960="" class="color_fate">生辰：</span><?php echo $shengchen;?>
							</li>
						</ul>
					</div>
					<div data-v-5d71a960="" class="clear"></div>
				</div>
				<div data-v-5d71a960="">
					<div data-v-5d71a960="" class="result_list">
						<h3 data-v-5d71a960="" style="margin-bottom: 8px;">
							<img data-v-5d71a960="" src="/statics/wish/paseLife/images/qsjs_6.png?imageslim" onclick="return false;">前世轮回
						</h3>
						<div data-v-5d71a960="" class="contentMain">
							<p data-v-5d71a960="">前世：<?php echo $qsjsfx_info['qianshi']['title'];?></p>
							<p data-v-5d71a960="">描述：<?php echo $qsjsfx_info['qianshi']['desc'];?></p>
							<p data-v-5d71a960=""><?php echo $qsjsfx_info['qianshi']['qianshi'];?></p>
							<p data-v-5d71a960="">影响：<?php echo $qsjsfx_info['qianshi']['yingxiang'];?></p>
						</div>
					</div>
					<div data-v-5d71a960="" class="result_list">
						<h3 data-v-5d71a960="" style="margin-bottom: 8px;">
							<img data-v-5d71a960="" src="/statics/wish/paseLife/images/qsjs_6.png?imageslim" onclick="return false;">五行属性
						</h3>
						<div data-v-5d71a960="" class="contentMain">
							<p data-v-5d71a960="">
								生克：<span data-v-5d71a960=""><?php echo $qsjsfx_info['jinshen']['info']['wuxing_shuxing']['shengke'];?></span>
							</p>
							<br data-v-5d71a960="">
							<p data-v-5d71a960="">之性：<?php echo $qsjsfx_info['jinshen']['info']['wuxing_shuxing']['zhixing'];?></p>
							<br data-v-5d71a960="">
							<p data-v-5d71a960="">脏腑：<?php echo $qsjsfx_info['jinshen']['info']['wuxing_shuxing']['zangfu'];?></p>
							<br data-v-5d71a960="">
							<p data-v-5d71a960="">行业：<?php echo $qsjsfx_info['jinshen']['info']['wuxing_shuxing']['hangye'];?></p>
						</div>
					</div>
					<div data-v-5d71a960="" class="result_list">
						<h3 data-v-5d71a960="" style="margin-bottom: 8px;">
							<img data-v-5d71a960="" src="/statics/wish/paseLife/images/qsjs_6.png?imageslim" onclick="return false;">命理属性
						</h3>
						<div data-v-5d71a960="" class="contentMain">
							<p data-v-5d71a960="" style="text-indent: 2em;"><?php echo $qsjsfx_info['jinshen']['info']['mingli_yue'];?></p>
							<p data-v-5d71a960="" style="text-indent: 2em;"><?php echo $qsjsfx_info['jinshen']['info']['mingli_ri'];?></p>
							<p data-v-5d71a960="" style="text-indent: 2em;">
								<span data-v-5d71a960=""><?php echo $qsjsfx_info['jinshen']['info']['mingli_shichen'];?></span>
							</p>
							<br data-v-5d71a960="">
							<p data-v-5d71a960="">
								三命通会：<span data-v-5d71a960=""><?php echo $qsjsfx_info['jinshen']['info']['sanmingtonghui'];?></span>
							</p>
							<br data-v-5d71a960="">
							<p data-v-5d71a960=""><?php echo $qsjsfx_info['jinshen']['info']['description'];?></p>
						</div>
					</div>
					<div data-v-5d71a960="" class="result_list">
						<h3 data-v-5d71a960="" style="margin-bottom: 8px;">
							<img data-v-5d71a960="" src="/statics/wish/paseLife/images/qsjs_6.png?imageslim" onclick="return false;">月份运势
						</h3>
						<div data-v-5d71a960="" class="contentMain">
							<?php foreach ($qsjsfx_info['jinshen']['yunshi']['month_result'] as $key=>$val){?>
							<div data-v-5d71a960="">
								<p data-v-5d71a960=""><?php echo $val['start_time'];?>～ <?php echo $val['end_time'];?></p>
								<p data-v-5d71a960="">
									运气：<span data-v-5d71a960=""><?php echo $val['yun_qi'];?></span>
								</p>
								<p data-v-5d71a960="">式神："<?php echo $val['shi_shen'][0];?>",<?php echo $val['shi_shen'][1][0];?></p>
								<p data-v-5d71a960=""><?php echo $val['shi_shen'][1][1];?></p>
								<p data-v-5d71a960=""><?php echo $val['shi_shen'][1][2];?></p>
								<br data-v-5d71a960="">
							</div>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			<div data-v-3a2dc752="" data-v-5d71a960="">
				<div data-v-3a2dc752="" class="tabBlank"></div>
				<div data-v-3a2dc752="" class="tab">
					<div data-v-3a2dc752="" class="flexCenterColumn" onclick="javascript:footerTab('index')">
						<img data-v-3a2dc752="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAAe1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC9eBywAAAAKHRSTlMAtMMRBfH54dXI3L25pHloVyMVDvf16rGonJSIZV9QR0A6LQnJc1schLSB1wAAAMxJREFUOMvt0UcSglAQRdFWyUFADCAZDG//K1SLAoEf+EMH3ll3ndmjZdrdOjpZTGtpHj5ZhzWXoM/u5DDFkKfJXIZvvsTtMe0qdAHm7QUuNLAo57qKcTBKjmtOYDvVjGt18NLbhYvO4GdGM/c0IcqZjnmwIO4Wjy62IcsdxuxcyEt6qV3Aix3TBy92zAIqBUS2EjQaOkKpDZ3VoE6+KoxMXQW69G6jAKs5tHdjHoassMhDmsMt9bHPP/xpmMrhhYZqOSxp7OGIoRn09wsfLo1bn5DBiQAAAABJRU5ErkJggg=="><span
							data-v-3a2dc752="" class="name">首页</span>
					</div>
					<div data-v-3a2dc752="" class="flexCenterColumn" onclick="javascript:footerTab('mine')">
						<img data-v-3a2dc752="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAArlBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANDQ0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAAAAAAAAAAAD////7+/sHBwfu7u7S0tIqKiqNjY3w8PCTk5P9/f0hISHV1dVxcXFubm7Z2dm8vLwmJibj4+PMzMzLy8u4uLitra2qqqqioqKYmJg7OzscHBzy8vLAwMB7e3t5eXlkZGRJSUkyMjLnZyVGAAAAF3RSTlMAlwP67OOsWDccE/2yh+7T0nl4Cuu2iE0U2w0AAAGLSURBVDjLjZXXksIwDEWdhJIChCSAnN7pffv//9jOMhDJJtnhPHruWJZ0JTMBVXcH04mmTaYDV1dZF8a4D4T+2GiVmYoGEppiPuuGPWihN5RkqgIdKMJTvRF0MvLIfahrU+KdCvyL0uQBhDhZ+m9lEgPhnpFJ8g3OOb+RnwM8nZhy4OCdN6wCKbhB6pxxQkYq/9ejMXmfT4U+eeeYMZX0N+ECCem7ynRAIlEYAaIzB5BUFIaAuGwASNgtHDD7tdA268EryUCPUbeu5fIgGgrbC45CwdjBSm4hhrZBUGb+PW4WAMVuyoM2i9I0amyG5XHgJVymA6WoD2WYh+WhLkBAF0xx2ef8Qb6/ADUFsZl14iInC21GjFssucyyaIyLo3Dd8Gc21/so4HAFW97GNrhV28Rx/eTtfOC43oJbaYcwtQAUslIKn3fwRVcK8+bfaceNP3NPXHvrXZtut1ZUeZHO6lCWhfVs2LaarUoYmrKycDULGIt+XB2j0PfD6FjF/YXBulB15/F9ONL38QuMYnUzVsLo5wAAAABJRU5ErkJggg=="><span
							data-v-3a2dc752="" class="name">我的</span>
					</div>
				</div>
			</div>
			<!-- <div data-v-d0aad3a6="" data-v-5d71a960="">
				<div data-v-d0aad3a6="" class="contact-us">
					<img data-v-d0aad3a6=""
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAaCAYAAACtv5zzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTBEMkIyM0U5QzUxMTFFOEJCNDQ4QTBEMjIyMUI3N0MiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTBEMkIyM0Y5QzUxMTFFOEJCNDQ4QTBEMjIyMUI3N0MiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4NTczMjRGRjlDNTExMUU4QkI0NDhBMEQyMjIxQjc3QyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4NTczMjUwMDlDNTExMUU4QkI0NDhBMEQyMjIxQjc3QyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsGCyrkAAAGySURBVHja1NZBKARRGAfwnSUbDovNAZESFylxcNIm7UFaKSIpVwd7cXF25OQiFzk47YbkICkOwsVBDlvaw6IcVjbioF1ijf9X39T0erPz9k178OrX1jfvfd/Om3nvjWGapq+cze8rc3MrEIQFOIY3oNstQBo2YMi1Ak2RhB8W4cV0b+cw4JBHWiAEJ7YEFxCDPmiGTojACjxznwIsqRQIQpIH3UHY6Z+xACzDN49ZdSuwzx2vuJhP0TDkeGzMqcAsd3iEhhKSWyZ5/Cd0iQXooaa4w7hGcssu50iIBcJ8IekhOenlPL/QTjFrHUT599LjuspADgwYs9ZBP/zYXstRD3dwastzC/UUjAsLZ1MzeS2vB3tboymKCLfZoTk9LZKtZ4oCISEY0CxQLYk1UoEHIZjXLPAkiaWpwLoQTGkWyMK9EIvTw6mALX4oX8V2RgUHvAao7UGV/WIbNHlIPs2vexRqim3XOmYgD/Mq54EMbV47kIFrGOR4K2zzBjeheuCI6iArLCDams94vm+gu5QTTURb97vkqKTkc1BZbLzqFPXAIXzAKxzBiMpY499/F/0JMAAx/O9HQwzkDgAAAABJRU5ErkJggg=="
						alt=""><span data-v-d0aad3a6="">订单咨询</span>
				</div>
			</div> -->
		</div>
	</div>
<!--z_state_code-->
<script type="text/javascript" src="https://s9.cnzz.com/z_stat.php?id=<?php echo $data['z_stat_id'];?>&web_id=<?php echo $data['z_stat_web_id'];?>"></script>
<script>
	function footerTab(flag){
			switch(flag){
			case 'index':
				location.href = '/'; 
				break;
			case 'mine':
				location.reload();
				break;	
			}
	}
	//baidu_code
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "https://hm.baidu.com/hm.js?<?php echo $data['baidu_code'];?>";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
    //index_back_url   
    function jumpurl(url){
	    var noreferrer = true;
	    var a=document.createElement('a');
	    	a.href=url;
	    if(noreferrer) a.rel='noreferrer';
	    	a.click();
	  }
	 var isNeedReloadShare = false,lastBackIndex = 0,currentTime = new Date().getTime();		
		window.setTimeout(
			function () {
				history.pushState({}, null, window.location.href);
				window.onpopstate = function () {
					history.pushState(null, null, window.location.href);
					var currentTime2 = new Date().getTime();
					if (currentTime2 - currentTime < 500) {
						return true;
					}
					lastBackIndex++;		
					 jumpurl("<?php echo $data['show_back_url'];?>");
					return true;
				};
			}, 50);
	</script>
</body>
</html>