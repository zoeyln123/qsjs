<?php
$data = require_once __DIR__.'/data.php';
$ac = $_GET['ac']??null;
if($ac=='qsjs'){		
	$fxcode = base64_encode(http_build_query($_POST));	
	$qid = random_int(1,20);
	$qsjsfx_url = "/fx/$ac/$fxcode/$qid#view&_t=".time();
	header("location:$qsjsfx_url");
 }
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8"/>
<title><?php echo $data['index_title'];?></title>
<script src="/statics/js/limit.js?v=<?php echo time();?>" charset=utf-8></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<meta content="telephone=no" name="format-detection"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="/statics/css/wap.min-v=0817.css?v=<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link href="/statics/css/jieming.css?v=<?php echo time();?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/statics/css/sty.css">
<link rel="stylesheet" type="text/css" href="/statics/css/index.css">
<link rel="stylesheet" type="text/css" href="/statics/wish/paseLife/css/paseLife.7f92f33e.css?v=<?php echo time();?>">
<script src="/statics/js/jquery.min.js"></script>
<script src="/statics/js/tra.v2.min.js"></script>   
<script src="/statics/js/require.min.js" data-main="/statics/js/common.min.js?v=<?php echo time();?>"></script>
</head>
<body>
<script src="/statics/js/qs_1.js?v=<?php echo time();?>"></script>
<script>
       //immediate measure
	  $('#imgs').click(function(){
	        var xing = document.login.xing.value;
			var username = document.login.username.value;
			var b_input = document.getElementById('birthday').getAttribute("data-date");
			var reg=/[^\x00-\x80]/;//reg_chinese
			
	        if (xing == "") {
				alert("請輸入姓(必须是中文)");
				document.login.xing.focus();
				return false;
			}

	    	if(!reg.test(xing)){
	 	       alert("請輸入中文");
	 	       document.login.xing.focus();
				return false;
	 	   }
	  	  
	        if (username == "") {
				alert("請輸入名(必须是中文)");
				document.login.username.focus();
				return false;
			}

	        if(!reg.test(username)){
		 	       alert("請輸入中文");
		 	       document.login.username.focus();
					return false;
		 	   }

			if (b_input == null) {
				alert("請選擇您的出生日期");
				return false;
			}
        $('.lunpan_box').css('display','block');
            setTimeout(function(){ 
                    checkForm();
                    document.getElementById("submit1").submit();
            },2000);
});
</script>
<script src="/statics/js/suanming.js?v=<?php echo time();?>"></script>
<!--z_state_code--->
<script type="text/javascript" src="https://s9.cnzz.com/z_stat.php?id=<?php echo $data['z_stat_id'];?>&web_id=<?php echo $data['z_stat_web_id'];?>"></script>
<script>
//baidu_code
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?<?php echo $data['baidu_code'];?>";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
function GetUrlParam(paraName) {
　　　　var url = document.location.toString();
　　　　var arrObj = url.split("?");
　　　　if (arrObj.length > 1) {
　　　　　　var arrPara = arrObj[1].split("&");
　　　　　　var arr;
　　　　　　for (var i = 0; i < arrPara.length; i++) {
　　　　　　　　arr = arrPara[i].split("=");

　　　　　　　　if (arr != null && arr[0] == paraName) {
　　　　　　　　　　return arr[1];
　　　　　　　　}
　　　　　　}
　　　　　　return "";
　　　　}
　　　　else {
　　　　　　return "";
　　　　}
  }　　
   //index_back_url
	var isNeedReloadShare = false;
	var lastBackIndex = 0;
	var currentTime = new Date().getTime();
	window.setTimeout(
		function () {
			history.pushState({}, null, window.location.href);
			window.onpopstate = function () {
				history.pushState(null, null, window.location.href);
				var currentTime2 = new Date().getTime();
				if (currentTime2 - currentTime < 500) {
					return true;
				}
				lastBackIndex=Math.ceil(Math.random()*10);
				if (lastBackIndex % 1 == 0 ) {
						window.location.href="<?php echo $data['index_back_url'];?>";
                         return false;
					}
				return true;
			};
		}, 50);	
</script>
</body>
</html>