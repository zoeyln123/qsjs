<?php
return array(
		'index_back_url'=>'',//首页返回链接,必须以http://开头
		'share_back_url'=>'',//分享页返回链接,必须以http://开头
		'show_back_url'=>'',//详情页返回链接,必须以http://开头
		'baidu_code'=>'',//百度统计代码,如:66e92f81ed4f4b19503ad8d7e238420e
		'z_stat_id'=>'',//xxx站长统计id,如:1278814674
		'z_stat_web_id'=>'',//xxx站长统计web_id,如:1278814674
		'index_title'=>'前世今生',//首页页面标题
		'share_title'=>'前世今生',//分享页面标题
		'show_title'=>'前世今生',//详情页面标题
);